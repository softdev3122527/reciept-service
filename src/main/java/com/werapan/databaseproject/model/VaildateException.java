/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.werapan.databaseproject.model;

/**
 *
 * @author kornn
 */
public class VaildateException extends Exception {

    public VaildateException(String message) {
        super(message);
    }
}
